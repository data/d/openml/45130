# OpenML dataset: barley_10

https://www.openml.org/d/45130

## Structure

The dataset has the following file structure:

* `dataset/`
  * `tables/`
    * [`data.pq`](./dataset/tables/data.pq): Parquet file with data
  * [`metadata.json`](./dataset/metadata.json): OpenML description of the dataset
  * [`features.json`](./dataset/features.json): OpenML description of table columns
  * [`qualities.json`](./dataset/qualities.json): OpenML qualities (meta-features)

## Description

**Barley Bayesian Network. Sample 10.**

bnlearn Bayesian Network Repository reference: [URL](https://www.bnlearn.com/bnrepository/discrete-medium.html#alarm)

- Number of nodes: 48

- Number of arcs: 84

- Number of parameters: 114005

- Average Markov blanket size: 5.25

- Average degree: 3.5

- Maximum in-degree: 4

**Authors**: Kristian Kristensen , Ilse A. Rasmussen and others

Preliminary model for barley developed under the project: "Production of beer from Danish malting barley grown without the use of pesticides" by Kristian Kristensen , Ilse A. Rasmussen and others.

## Contributing

This is a [read-only mirror](https://gitlab.com/data/d/openml/45130) of an [OpenML dataset](https://www.openml.org/d/45130). Contribute any changes to the dataset there. Alternatively, [fork the dataset](https://gitlab.com/data/d/openml/45130/-/forks/new) or [find an existing fork](https://gitlab.com/data/d/openml/45130/-/forks) to contribute to.

You can use [issues](https://gitlab.com/data/d/openml/45130/-/issues) to discuss the dataset and any issues.

For more information see [https://datagit.org/](https://datagit.org/).

